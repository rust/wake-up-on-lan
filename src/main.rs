// Lof dependencies
extern crate log;
use log::{info, LevelFilter};
use env_logger::{Builder, Target};

// Configuration dependencies
extern crate config;
extern crate serde;
#[macro_use]
extern crate serde_derive;


mod settings;
use settings::Settings;

use wake_on_lan;
use wake_on_lan::MagicPacket;

use get_if_addrs::{Interface, IfAddr , get_if_addrs, Ifv6Addr};
use std::net::{SocketAddrV4, SocketAddrV6};


fn main() {
    // Manage the Log
    let mut builder = Builder::from_default_env();
    builder.target(Target::Stdout);
    builder.filter_level(LevelFilter::Debug);
    builder.init();
    // Read the configuration
    let settings = Settings::new().expect("Can't read the configuration");
    println!("Configuration settings is {:?}", settings);

    let ifaces = get_if_addrs().unwrap();

    // The MAC address of the target device
    let mac_addr = to_mac(&settings.wol.mac);
    info!("WOL for the mac : {:?}", &mac_addr);

    for ifs in ifaces {
        let magic_packet = packet_from(&mac_addr);
        match ifs {
            Interface {name , addr } => { 
                match addr {
                    IfAddr::V4 (ifv4) => {
                        info!("Send WOL v4 from : {:#?}  -  {:#?}", name, ifv4.ip);
                        let from_socket =  SocketAddrV4::new(ifv4.ip, 0); 
                        let res = magic_packet.send_from_v4(from_socket);
                        print_error_if_needed(&mac_addr, res);
                    },
                    IfAddr::V6 (ifv6) => {
                        info!("Send WOL v6 from : {:#?}  -  {:#?}", name, ifv6.ip); 
                        wol_from_v6(&mac_addr, magic_packet, ifv6);
                    }
                }
            }
        }
        
    }
}

fn print_error_if_needed(mac_address: &[u8; 6], res: std::io::Result<()>) {
    match res {
        Result::Err(e) => {
            info!("Can't send magic packet to {:?}. {:?}", mac_address, e);
        },
        Result::Ok(_) => {();}
    }
}

fn to_mac(mac_str: &str) -> [u8; 6] {
    let mac_1: Vec<&str> = mac_str
        .split(':')
        .collect();
    let mac: Vec<u8> = mac_1
        .iter().map(|s | {
            u8::from_str_radix(s, 16).expect("Can't parse mac address")
        }).collect();
    
    let mut res: [u8; 6] = [0; 6];
    for i in 0..6 {
        res[i] = mac[i];
    }

    return res;
}

fn packet_from(mac_address: &[u8; 6])-> MagicPacket {
    // Create a magic packet (but don't send it yet)
    wake_on_lan::MagicPacket::new(mac_address)
}


fn wol_from_v6(mac_address: &[u8; 6], magic_packet: MagicPacket, ifv6: Ifv6Addr) {
    // Send the magic packet via UDP to the broadcast address 255.255.255.255:9 
    // from 0.0.0.0:0
    let from_socket = SocketAddrV6::new(ifv6.ip, 0, 0, 0);
    ifv6.broadcast.map({ |bc| 
        for _i in 1..100 {
            let bc_socket = SocketAddrV6::new(bc, 9, 0, 0);
            let res = magic_packet.send_from_v6(from_socket, bc_socket);
            print_error_if_needed(mac_address, res);
        }
    });
}